export interface Color {
  colorCode: string;
  additionalColorCode: string;
}
