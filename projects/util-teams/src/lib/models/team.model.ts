import { Color } from "./color.model";
import { TeamMember } from "./team-member.model";

export interface Team {
  teamMembers: TeamMember[];
  color: Color;
  teamImageUrl: string;
}
