import { Component } from "@angular/core";
import { Team, TeamMember } from "projects/util-teams/src/public-api";
import { Settings } from "../settings/settings.model";

@Component({
  selector: "lib-feat-teams-generator",
  templateUrl: `./feat-teams-generator.component.html`,
  styleUrls: ["./feat-teams-generator.component.scss"],
})
export class FeatTeamsGeneratorComponent {
  public members: TeamMember[] = [];
  public teams: Team[] = [];

  public onAddMember(teamMember: TeamMember): void {
    this.members.push(teamMember);
  }

  public onRemove(index: number): void {
    this.members.splice(index, 1);
  }

  public onGenerate(settings: Settings): void {
    this.teams = [];
    for (let i = 0; i < settings.numberOfTeams; i++) {
      this.teams.push({
        color: { colorCode: "222222", additionalColorCode: "333333" },
        teamImageUrl: `https://cataas.com/cat/gif?cb=${Math.round(
          Math.random() * 1000
        )}`,
        teamMembers: [{ name: "Member 1" }, { name: "Member 2" }],
      });
    }

    this.members.forEach((member) => {
      const randomGroupId = Math.floor(Math.random() * this.teams.length);
      this.teams[randomGroupId].teamMembers.push(member);
    });

    this.showConfetti();
  }

  public showConfetti(): void {
    alert("Show confetti");
  }

  private _getRandomColor(): string {
    var letters = "0123456789ABCDEF";
    var color = "#";
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
}
