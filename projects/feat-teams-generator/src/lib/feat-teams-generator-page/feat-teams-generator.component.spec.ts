import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeatTeamsGeneratorComponent } from './feat-teams-generator.component';

describe('FeatTeamsGeneratorComponent', () => {
  let component: FeatTeamsGeneratorComponent;
  let fixture: ComponentFixture<FeatTeamsGeneratorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FeatTeamsGeneratorComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FeatTeamsGeneratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
