import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MembersModule } from "../members/members.module";
import { SettingsModule } from "../settings/settings.module";
import { TeamModule } from "../teams/team.module";
import { FeatTeamsGeneratorComponent } from "./feat-teams-generator.component";

@NgModule({
  imports: [SettingsModule, CommonModule, MembersModule, TeamModule],
  declarations: [FeatTeamsGeneratorComponent],
  exports: [FeatTeamsGeneratorComponent],
})
export class FeatTeamsGeneratorPageModule {}
