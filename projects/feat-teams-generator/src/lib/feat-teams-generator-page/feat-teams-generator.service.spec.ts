import { TestBed } from '@angular/core/testing';

import { FeatTeamsGeneratorService } from './feat-teams-generator.service';

describe('FeatTeamsGeneratorService', () => {
  let service: FeatTeamsGeneratorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FeatTeamsGeneratorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
