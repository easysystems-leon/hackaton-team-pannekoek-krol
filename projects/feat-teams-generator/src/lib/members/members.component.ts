import { Component, EventEmitter, Input, Output } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { TeamMember } from "projects/util-teams/src/public-api";

@Component({
  selector: "lib-members",
  templateUrl: `./members.component.html`,
  styleUrls: ["members.component.scss"],
})
export class MembersComponent {
  @Input() members: TeamMember[] = [];
  @Output() memberAdded: EventEmitter<TeamMember> =
    new EventEmitter<TeamMember>();
  @Output() memberRemoved: EventEmitter<number> = new EventEmitter<number>();

  public formGroup: FormGroup = new FormGroup({
    memberName: new FormControl(),
  });
  public onAdd(): void {
    this.memberAdded.next({ name: this.formGroup.get("memberName")?.value });
    this.formGroup.reset();
  }

  public onRemove(index: number): void {
    this.memberRemoved.next(index);
  }
}
