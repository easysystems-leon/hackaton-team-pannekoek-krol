import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FeatTeamsGeneratorComponent } from "./feat-teams-generator-page/feat-teams-generator.component";
import { FeatTeamsGeneratorPageModule } from "./feat-teams-generator-page/feat-teams-generator.module";

@NgModule({
  imports: [
    FeatTeamsGeneratorPageModule,
    RouterModule.forChild([
      { path: "", component: FeatTeamsGeneratorComponent },
    ]),
  ],
})
export class FeatTeamsGeneratorModule {}
