import { Component, EventEmitter, Output } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { Settings } from "./settings.model";

@Component({
  selector: "lib-settings",
  templateUrl: `./settings.component.html`,
  styles: [],
})
export class SettingsComponent {
  @Output() generate: EventEmitter<Settings> = new EventEmitter<Settings>();

  public formGroup: FormGroup = new FormGroup({
    nrOfTeams: new FormControl("4"),
  });

  public onGenerate(): void {
    this.generate.next({
      numberOfTeams: this.formGroup.get("nrOfTeams")?.value,
    });
  }
}
