import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SettingsComponent } from "./settings.component";

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule],
  declarations: [SettingsComponent],
  exports: [SettingsComponent],
})
export class SettingsModule {}
