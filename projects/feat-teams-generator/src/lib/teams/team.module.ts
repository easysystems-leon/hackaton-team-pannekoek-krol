import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { TeamComponent } from "./team.component";

@NgModule({
  imports: [CommonModule],
  declarations: [TeamComponent],
  exports: [TeamComponent],
})
export class TeamModule {}
