import { Component, Input } from "@angular/core";
import { Team } from "projects/util-teams/src/public-api";

@Component({
  selector: "lib-team",
  templateUrl: `./team.component.html`,
  styleUrls: ["./team.component.scss"],
})
export class TeamComponent {
  @Input() team: Team = {
    color: { additionalColorCode: "ffffff", colorCode: "ffffff" },
    teamImageUrl: "",
    teamMembers: [],
  };
}
