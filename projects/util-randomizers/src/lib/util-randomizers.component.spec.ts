import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UtilRandomizersComponent } from './util-randomizers.component';

describe('UtilRandomizersComponent', () => {
  let component: UtilRandomizersComponent;
  let fixture: ComponentFixture<UtilRandomizersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UtilRandomizersComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UtilRandomizersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
