import { TestBed } from '@angular/core/testing';

import { UtilRandomizersService } from './util-randomizers.service';

describe('UtilRandomizersService', () => {
  let service: UtilRandomizersService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UtilRandomizersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
