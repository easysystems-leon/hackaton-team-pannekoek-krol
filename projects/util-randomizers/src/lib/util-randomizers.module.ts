import { NgModule } from '@angular/core';
import { UtilRandomizersComponent } from './util-randomizers.component';



@NgModule({
  declarations: [
    UtilRandomizersComponent
  ],
  imports: [
  ],
  exports: [
    UtilRandomizersComponent
  ]
})
export class UtilRandomizersModule { }
