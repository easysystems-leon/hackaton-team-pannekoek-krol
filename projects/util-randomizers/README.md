# UtilRandomizers

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.2.0.

## Code scaffolding

Run `ng generate component component-name --project util-randomizers` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project util-randomizers`.
> Note: Don't forget to add `--project util-randomizers` or else it will be added to the default project in your `angular.json` file. 

## Build

Run `ng build util-randomizers` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `ng build util-randomizers`, go to the dist folder `cd dist/util-randomizers` and run `npm publish`.

## Running unit tests

Run `ng test util-randomizers` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
